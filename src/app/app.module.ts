import {BrowserModule} from '@angular/platform-browser';
import {LOCALE_ID, NgModule} from '@angular/core';

import {AppRoutingModule} from './app-routing.module';
import {AppComponent} from './app.component';
import {PagesModule} from './pages/pages.module';
import {CoreModule} from './core/core.module';
import {HTTP_INTERCEPTORS, HttpClientModule} from '@angular/common/http';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {LayoutModule} from './layout/layout.module';
import {NgZorroAntdModule, NZ_I18N, NzDatePickerModule, pt_BR} from 'ng-zorro-antd';
import localePt from '@angular/common/locales/pt';
import {OverlayModule} from '@angular/cdk/overlay';
import {registerLocaleData} from '@angular/common';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import {RequestInterceptor} from './core/auth/request.interceptor';

registerLocaleData(localePt);

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    PagesModule,
    ReactiveFormsModule,
    FormsModule,
    HttpClientModule,
    CoreModule,
    NgZorroAntdModule,
    BrowserAnimationsModule,
    OverlayModule,
    LayoutModule,
    NzDatePickerModule,
    AppRoutingModule
  ],
  providers: [
    {
      provide: HTTP_INTERCEPTORS,
      useClass: RequestInterceptor,
      multi: true
    },
    { provide: NZ_I18N, useValue: pt_BR },
    { provide: LOCALE_ID, useValue: 'pt-BR' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
