import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {PessoaModule} from './pessoa/pessoa.module';
import {RouterModule, Routes} from '@angular/router';


const pagesRoutes: Routes = [
];

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PessoaModule,
    RouterModule.forChild(pagesRoutes),
  ]
})
export class PagesModule { }
