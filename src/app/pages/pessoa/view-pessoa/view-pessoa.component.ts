import {Component, OnInit} from '@angular/core';
import {Pessoa} from '../../../core/model/pessoa';
import {PessoaService} from '../../../core/services/pessoa/pessoa.service';
import {ActivatedRoute} from '@angular/router';
import { registerLocaleData } from '@angular/common';
import pt from '@angular/common/locales/pt';

@Component({
  selector: 'app-view-pessoa',
  templateUrl: './view-pessoa.component.html',
  styleUrls: ['./view-pessoa.component.sass']
})
export class ViewPessoaComponent implements OnInit {

  private idPessoa;
  pessoa = {} as Pessoa;

  constructor(
    private pessoaService: PessoaService,
    private activatedRoute: ActivatedRoute) {
  }

  ngOnInit() {
    this.getPessoaById();
    registerLocaleData(pt);
  }

  getPessoaById() {
    this.idPessoa = this.activatedRoute.snapshot.params.uuid;
    this.pessoaService.getPessoaById(this.idPessoa)
      .subscribe(data => {
          this.pessoa = data;
        },
        error => console.log(error)
      );
  }


}
