import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ListPessoaComponent} from './list-pessoa/list-pessoa.component';
import {RouterModule, Routes} from '@angular/router';
import {AddPessoaComponent} from './add-pessoa/add-pessoa.component';
import {EditPessoaComponent} from './edit-pessoa/edit-pessoa.component';
import {ViewPessoaComponent} from './view-pessoa/view-pessoa.component';
import {NzDatePickerModule, NzIconModule, NzPopconfirmModule} from 'ng-zorro-antd';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {AuthGuard} from '../../core/auth/auth.guard';

const pessoaRoutes: Routes = [
  {path: 'list-pessoa', component: ListPessoaComponent, canActivate: [AuthGuard], canLoad: [AuthGuard] },
  {path: 'add-pessoa', component: AddPessoaComponent, canActivate: [AuthGuard], canLoad: [AuthGuard]},
  {path: 'edit-pessoa/:uuid', component: EditPessoaComponent, canActivate: [AuthGuard], canLoad: [AuthGuard]},
  {path: 'view-pessoa/:uuid', component: ViewPessoaComponent, canActivate: [AuthGuard], canLoad: [AuthGuard]},
];


@NgModule({
  declarations: [
    ListPessoaComponent,
    AddPessoaComponent,
    EditPessoaComponent,
    ViewPessoaComponent],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forChild(pessoaRoutes),
    NzDatePickerModule,
    NzPopconfirmModule,
    NzIconModule,
  ]
})
export class PessoaModule { }
