import {Component, OnInit} from '@angular/core';
import {PessoaService} from '../../../core/services/pessoa/pessoa.service';
import {Pessoa} from '../../../core/model/pessoa';
import {NzMessageService} from 'ng-zorro-antd';


@Component({
  selector: 'app-list-pessoa',
  templateUrl: './list-pessoa.component.html',
  styleUrls: ['./list-pessoa.component.sass']
})
export class ListPessoaComponent implements OnInit {

  private page = 0;
  private size = 10;
  pessoas: Pessoa[];

  constructor(
    private pessoaService: PessoaService,
    private toaster: NzMessageService) {
  }


  ngOnInit() {
    this.listPessoa();
  }

  listPessoa() {
    this.pessoaService.getPessoa(this.page, this.size)
      .subscribe(data => {
        this.pessoas = data.content as Pessoa[];
      }, error => {
        console.log(error);
      });
  }

  deletar(uuid: string) {
    this.pessoaService.deletePessoa(uuid).subscribe(() => {
        this.toaster.success('Registro deletado com sucesso');
        this.listPessoa();
    }, error => console.log(error)
    );
  }


}
