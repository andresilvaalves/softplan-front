import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Pessoa} from '../../../core/model/pessoa';
import {PessoaService} from '../../../core/services/pessoa/pessoa.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd';
import {Sexo} from '../../../core/model/sexo';

@Component({
  selector: 'app-edit-pessoa',
  templateUrl: './edit-pessoa.component.html',
  styleUrls: ['./edit-pessoa.component.sass']
})
export class EditPessoaComponent implements OnInit {

  pessoaForm: FormGroup;
  private idPessoa;
  pessoa = {} as Pessoa;

  constructor(private pessoaService: PessoaService,
              private formBuilder: FormBuilder,
              private router: Router,
              private toaster: NzMessageService,
              private activatedRoute: ActivatedRoute) { }

  ngOnInit() {
    this.pessoaForm = this.formBuilder.group({
      nome: ['', Validators.required],
      cpf: ['', [Validators.required] ],
      email: ['', [Validators.email, Validators.required] ],
      nascimento: ['', Validators.required],
      naturalidade: '',
      nacionalidade: '',
      sexo: ['', [Validators.required] ],
      uuid: ''
    });

    this.getPessoaById();
  }

  getPessoaById() {
    this.idPessoa = this.activatedRoute.snapshot.params.uuid;
    this.pessoaService.getPessoaById(this.idPessoa)
      .subscribe(data => {
          this.setPessoa(data);
        },
        error => {
          console.log(error);
          this.toaster.error('Erro ao buscar Pessoa');
        }
      );
  }

  salvar() {
    this.pessoa = this.pessoaForm.getRawValue() as Pessoa;
    this.pessoa.sexo = {} as Sexo;
    this.pessoa.sexo.nome = this.pessoaForm.get('sexo').value;

    this.pessoaService.updatePessoa(this.pessoa)
      .subscribe(data => {
        this.pessoa = data as Pessoa;
        this.router.navigate(['list-pessoa']);
        this.toaster.success('Pessoa atualizada com sucesso!');
      }, e => {
        console.log(e);
        this.toaster.error('Erro ao salvar Pessoa');
      });
  }

  private setPessoa(pessoa: Pessoa) {
    this.pessoa = pessoa;
    this.pessoaForm.patchValue({nome: this.pessoa.nome});
    this.pessoaForm.patchValue({id: this.pessoa.uuid});
    this.pessoaForm.patchValue({cpf: this.pessoa.cpf});
    this.pessoaForm.patchValue({email: this.pessoa.email});
    this.pessoaForm.patchValue({nascimento: this.pessoa.nascimento});
    this.pessoaForm.patchValue({sexo: this.pessoa.sexo.descricao});
    this.pessoaForm.patchValue({naturalidade: this.pessoa.naturalidade});
    this.pessoaForm.patchValue({nacionalidade: this.pessoa.nacionalidade});
    this.pessoaForm.patchValue({uuid: this.pessoa.uuid});
  }

}
