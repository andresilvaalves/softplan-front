import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Pessoa} from '../../../core/model/pessoa';
import {PessoaService} from '../../../core/services/pessoa/pessoa.service';
import {ActivatedRoute, Router} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd';
import {Sexo} from '../../../core/model/sexo';

@Component({
  selector: 'app-add-pessoa',
  templateUrl: './add-pessoa.component.html',
  styleUrls: ['./add-pessoa.component.sass']
})
export class AddPessoaComponent implements OnInit {

  pessoaForm: FormGroup;
  pessoa = {} as Pessoa;


  constructor(private pessoaService: PessoaService,
              private formBuilder: FormBuilder,
              private router: Router,
              private toaster: NzMessageService,
              private activatedRoute: ActivatedRoute
  ) { }

  ngOnInit() {
    this.pessoaForm = this.formBuilder.group({
      nome: ['', Validators.required],
      cpf: ['', [Validators.required] ],
      email: ['', [Validators.email, Validators.required] ],
      nascimento: ['', Validators.required],
      naturalidade: '',
      nacionalidade: '',
      sexo: ['', [Validators.required] ],
      uuid: ''
    });
  }

  salvar() {
    this.pessoa = this.pessoaForm.getRawValue() as Pessoa;
    this.pessoa.sexo = {} as Sexo;
    this.pessoa.sexo.nome = this.pessoaForm.get('sexo').value;

    console.log(this.pessoa);
    this.pessoaService.createPessoa(this.pessoa)
      .subscribe(data => {
        this.pessoa = data as Pessoa;
        this.router.navigate(['list-pessoa']);
        this.toaster.success('Pessoa salva com sucesso!');
      }, e => {
        console.log(e);
        this.toaster.error('Erro ao salvar Pessoa');
      });

  }

}
