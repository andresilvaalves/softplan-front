import {Injectable, isDevMode} from '@angular/core';
import {environment as prod} from '../environments/environment.prod';
import {environment as dev} from '../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class Globals {

  public readonly API_URL: string = isDevMode ? dev.API_URL : prod.API_URL;

  public readonly REGEX_CEP = /^(\d{5})(\d{3}).*/;
  public readonly REGEX_CPF = /^(\d{3})(\d{3})(\d{3})(\d{2}).*/;
  public readonly REGEX_CNPJ = /^(\d{2})(\d{3})(\d{3})(\d{4})(\d{2}).*/;

  public readonly REGEX_CELULAR = /^(\d{2})(\d{4,4})(\d{4})/;
  public readonly REGEX_FIXO = /^(\d{2})(\d{4,4})(\d{4})/;

}
