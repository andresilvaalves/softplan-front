import {Injectable} from '@angular/core';
import {tap} from 'rxjs/operators';
import {HttpClient, HttpHeaders} from '@angular/common/http';

import {Globals} from '../../globals';
import {UserService} from '../services/user/user.service';

@Injectable({
  providedIn: 'root',
})
export class AuthService {

  constructor(
    private http: HttpClient,
    private globals: Globals,
    private userService: UserService) {
  }

  authenticate(username: string, password: string) {
    console.log('authenticate');
    const headerss = new HttpHeaders({
      'Content-Type': 'application/json',
    });
    const options = {headerss};

    return this.http
      .post(
        this.globals.API_URL + '/login',
        {username, password},
        {
          headers: headerss,
          observe: 'response'
        },
      )
      .pipe(tap(res => {
        const authToken = res.headers.get('x-access-token');

        if (authToken) {
          this.userService.setToken(authToken);
        } else {
          console.log('sem authToken');
        }
      }));
  }



}
