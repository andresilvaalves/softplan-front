import {Moment} from 'moment';
import {Sexo} from './sexo';

export interface Pessoa {

  uuid: string;
  nome: string;
  cpf: string;
  sexo: Sexo;
  email: string;
  nascimento: Moment;
  naturalidade: string;
  nacionalidade: string;

}
