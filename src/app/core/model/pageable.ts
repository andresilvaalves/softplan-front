export interface Pageable {
  content: any[];
  totalElements: number;
  totalPages: number;
  size: number;
  numberOfElements: number;
  first: boolean;
  last: boolean;

}
