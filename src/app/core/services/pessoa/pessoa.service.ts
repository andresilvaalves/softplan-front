import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Globals} from '../../../globals';
import {Pageable} from '../../model/pageable';
import {Pessoa} from '../../model/pessoa';

@Injectable({
  providedIn: 'root'
})
export class PessoaService {

  private baseUrl: string = this.globals.API_URL + '/pessoa';

  constructor( private http: HttpClient,
               private globals: Globals) { }

  getPessoa(page: number, size: number) {
    return this.http.get<Pageable>(this.baseUrl + '?page=' + page + '&size=' + size);
  }

  getPessoaById(uuid: string) {
    return this.http.get<Pessoa>(this.baseUrl + '/' + uuid);
  }

  createPessoa(pessoa: Pessoa) {
    return this.http.post(this.baseUrl, pessoa);
  }

  updatePessoa(pessoa: Pessoa) {
    return this.http.put(this.baseUrl, pessoa);
  }
  deletePessoa(uuid: string) {
    return this.http.delete(this.baseUrl + '/' + uuid);
  }

}
