import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {ActivatedRoute, Router} from '@angular/router';
import {NzMessageService} from 'ng-zorro-antd';
import {AuthService} from '../../core/auth/auth.service';
import {UserService} from '../../core/services/user/user.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.sass']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private formBuilder: FormBuilder,
              private router: Router,
              private toaster: NzMessageService,
              private activatedRoute: ActivatedRoute,
              public userService: UserService,
              private authService: AuthService) { }

  ngOnInit() {
    this.loginForm = this.formBuilder.group({
      userName: ['', [Validators.required]],
      password: ['', Validators.required]
    });
  }

  login() {
    const userName =  this.loginForm.get('userName').value;
    const password = this.loginForm.get('password').value;

    this.authService
      .authenticate(userName, password)
      .subscribe(data => {
          this.toaster.success('Seja bem vindo ao sistema');
          this.router.navigate(['/home']);
        },
        err => {
          console.log(err);
          this.toaster.error('Erro ao fazer login', );
          this.loginForm.reset();
        });
  }

}
